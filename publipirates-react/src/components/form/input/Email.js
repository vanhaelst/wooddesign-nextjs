import React from 'react';
import Input from "@/components/form/InputField";

const EmailInput = ({ label }) => {
    return(
        <Input label={label} />
    )
}

export default EmailInput;
