import React from 'react';
import Input from "@/components/form/InputField";

const TextInput = ({ label }) => {
    return(
        <Input label={label} />
    )
}

export default TextInput;
