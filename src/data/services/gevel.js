import React, { Fragment } from 'react';

const gevel = [
    {
        index: 1,
        title: "Houten gevels",
        image: "https://www.wooddesign.be/wp-content/uploads/2016/06/IMG_4099.jpeg",
        description: "Open Gevelsysteem: gevellatten, planken of balken met open voeg, steeds geplaatst met een zwart regenscherm.  Gesloten Gevelsysteem: planchetten en blokprofielen met tand en groef. Houtsoorten: Afrormosia, Kebony, Padouk, Thermo Essen, Accoya, Thermo Vuren, Douglas,….",
    },
    {
        index: 2,
        title: "Plaatgevels & leien",
        image: "https://www.wooddesign.be/wp-content/uploads/2016/06/IMG_4099.jpeg",
        description: <Fragment>
                Eternit : Equitone Tectiva, Natura,…, Cedral Sidings, Vezelcementleien https://www.eternit.be/nl/gevel/<br />
                Trespa : Meteon Unicolours, Metallics, Wood Decors, Naturals https://www.trespa.com/nl<br />
                Rockpanel: Colours, Woods, Stones, Metalics, Lines,…  https://www.rockpanel.be
            </Fragment>
    }
]

export default gevel;
