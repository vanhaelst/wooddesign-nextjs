const navigation = [
    {
        title: "Home",
        href: "/"
    },
    {
        title: "Parket",
        href: "/parket"
    },
    {
        title: "Gevel",
        href: "/gevel"
    },
    {
        title: "Terras",
        href: "/terras"
    },
    {
        title: "Wand",
        href: "/wand"
    },
    {
        title: "Realisaties",
        href: "/realisaties"
    },
    {
        title: "Contact",
        href: "/contact"
    },
]


export default navigation;
